package data;

import exception.RequirementNotFoundException;
import model.Requirement;
import model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class RequirementOperations {

    private static int IdAccumulator = 0;
    private static HashMap<Integer, Requirement> Requirements = new HashMap<>();

    public Collection<Requirement> getRequirements() {
        return Requirements.values();
    }

    public int createRequirement(String name, String description) {
        Integer id = IdAccumulator;
        IdAccumulator++;

        Requirements.put(id, new Requirement(id, name, description));

        return id;
    }

    public void createApplication(User user, int requirementId) throws Exception {
        if (Requirements.containsKey(requirementId)) {
            Requirement requirement = Requirements.get(requirementId);
            ArrayList<User> applicants = requirement.getApplicants();

            if (!applicants.contains(user)) {
                applicants.add(user);
            }
        } else {
            throw new RequirementNotFoundException();
        }
    }
}
