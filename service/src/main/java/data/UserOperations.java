package data;

import exception.InvalidCredentialsException;
import exception.UserNotFoundException;
import model.User;
import model.UserRole;

import java.util.HashMap;
import java.util.UUID;

public class UserOperations {
    private static HashMap<String, User> Users = new HashMap<>();
    private static HashMap<String, User> Sessions = new HashMap<>();

    static {
        Users.put("admin", new User(0, "admin", "password", UserRole.ADMIN));
        Users.put("applicant_a", new User(1, "applicant_a", "password", UserRole.APPLICANT));
        Users.put("applicant_b", new User(2, "applicant_b", "password", UserRole.APPLICANT));
    }

    public User authenticate(String username, String password) throws Exception {
        if (Users.containsKey(username)) {
            User user = Users.get(username);

            if (user.getPassword().equals(password)) {
                return user;
            } else {
                throw new InvalidCredentialsException();
            }
        } else {
            throw new UserNotFoundException();
        }
    }

    public User authenticate(String token) throws Exception {
        if (Sessions.containsKey(token)) {
            return Sessions.get(token);
        } else {
            throw new InvalidCredentialsException();
        }
    }

    public String issueToken(User user) {
        String token = UUID.randomUUID().toString();

        Sessions.put(token, user);

        return token;
    }

    public User getUserByUsername(String username) throws Exception {
        if (Users.containsKey(username)) {
            return Users.get(username);
        } else {
            throw new UserNotFoundException();
        }
    }
}
