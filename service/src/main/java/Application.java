import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import rest.RestConfig;

import javax.ws.rs.core.UriBuilder;

public class Application {

    public static void main(String[] args) {
        Server server = JettyHttpContainerFactory.createServer(
                UriBuilder.fromUri("http://localhost:9090").build(),
                new RestConfig(),
                false
        );

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
