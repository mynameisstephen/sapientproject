package exception;

public class RequirementNotFoundException extends Exception {
    public RequirementNotFoundException() {
        super("Requirement not found");
    }
}
