package exception;

public class UserNotFoundException extends Exception {
    public UserNotFoundException() {
        super("UserDescription not found");
    }
}
