package rest.mapper;

import exception.RequirementNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RequirementNotFoundMapper implements ExceptionMapper<RequirementNotFoundException> {
    @Override
    public Response toResponse(RequirementNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
