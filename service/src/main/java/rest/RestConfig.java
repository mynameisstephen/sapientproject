package rest;

import data.RequirementOperations;
import data.UserOperations;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

public class RestConfig extends ResourceConfig {
    public RestConfig() {
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(UserOperations.class).to(UserOperations.class);
                bind(RequirementOperations.class).to(RequirementOperations.class);
            }
        });

        packages(this.getClass().getPackage().getName());
    }
}
