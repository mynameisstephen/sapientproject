package rest.auth;

import data.UserOperations;
import model.User;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Principal;

@Secured
@Priority(Priorities.AUTHENTICATION)
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {
    public static final String AUTHENTICATION_SCHEME = "Bearer";

    @Inject
    private UserOperations userOperations;

    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String authorization = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorization != null && authorization.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase())) {
            String token = authorization.substring(AUTHENTICATION_SCHEME.length() + 1);

            try {
                User user = userOperations.authenticate(token);
                final SecurityContext context = containerRequestContext.getSecurityContext();

                containerRequestContext.setSecurityContext(new SecurityContext() {
                    public Principal getUserPrincipal() {
                        return user::getUsername;
                    }

                    public boolean isUserInRole(String role) {
                        return user.getRole().toString().equals(role);
                    }

                    public boolean isSecure() {
                        return context.isSecure();
                    }

                    public String getAuthenticationScheme() {
                        return AUTHENTICATION_SCHEME;
                    }
                });

            } catch (Exception e) {
                containerRequestContext.abortWith(
                        Response.status(Response.Status.UNAUTHORIZED).build()
                );
            }
        } else {
            containerRequestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED).build()
            );
        }
    }
}

