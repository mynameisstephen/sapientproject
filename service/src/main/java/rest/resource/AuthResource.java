package rest.resource;

import data.UserOperations;
import model.User;
import rest.model.LoginRequest;
import rest.model.LoginResponse;
import rest.translator.UserTranslator;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes(MediaType.APPLICATION_JSON)
@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {

    @Inject
    private UserOperations userOperations;

    @POST
    public Response login(LoginRequest request) {
        try {
            User user = userOperations.authenticate(
                    request.getUsername(),
                    request.getPassword()
            );

            String token = userOperations.issueToken(user);

            return Response.ok(
                    new LoginResponse(
                            token,
                            UserTranslator.translate(user)
                    )
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }
}
