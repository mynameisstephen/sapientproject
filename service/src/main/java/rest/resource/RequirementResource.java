package rest.resource;

import com.google.gson.Gson;
import data.RequirementOperations;
import data.UserOperations;
import model.Requirement;
import model.UserRole;
import rest.auth.Secured;
import rest.model.CreateRequirementRequest;
import rest.model.CreateRequirementResponse;
import rest.translator.RequirementTranslator;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.Collection;

@Consumes(MediaType.APPLICATION_JSON)
@Path("/requirement")
@Produces(MediaType.APPLICATION_JSON)
public class RequirementResource {

    @Inject
    private RequirementOperations requirementOperations;

    @Inject
    private UserOperations userOperations;

    @GET
    @Secured
    public Response list(@Context SecurityContext securityContext) {
        boolean isAdmin = securityContext.isUserInRole(UserRole.ADMIN.toString());

        Collection<Requirement> requirements = requirementOperations.getRequirements();

        return Response.status(200).entity(
                new Gson().toJson(RequirementTranslator.translate(requirements, isAdmin))
        ).build();
    }

    @POST
    @Secured
    public Response create(
            CreateRequirementRequest request,
            @Context SecurityContext securityContext
    ) {
        int id = requirementOperations.createRequirement(request.getName(), request.getDescription());
        CreateRequirementResponse response = new CreateRequirementResponse(id);

        return Response.status(200).entity(response).build();
    }

    @POST
    @Path("/{requirementId}/apply")
    @Secured
    public Response apply(
            @PathParam("requirementId") int requirement,
            @Context SecurityContext securityContext
    ) throws Exception {
        requirementOperations.createApplication(
                userOperations.getUserByUsername(securityContext.getUserPrincipal().getName()),
                requirement
        );

        return Response.status(200).build();
    }
}
