package rest.model;

public class CreateRequirementResponse {
    private final int id;

    public CreateRequirementResponse(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
