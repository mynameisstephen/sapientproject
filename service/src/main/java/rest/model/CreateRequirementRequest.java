package rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateRequirementRequest {
    private final String name;
    private final String description;

    public CreateRequirementRequest(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description
    ) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
