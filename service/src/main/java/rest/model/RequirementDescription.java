package rest.model;

import java.util.ArrayList;

public class RequirementDescription {

    private final int id;
    private final String name;
    private final String description;
    private final ArrayList<UserDescription> applicants;

    public RequirementDescription(int id, String name, String description, ArrayList<UserDescription> applicants) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.applicants = applicants;
    }

    public RequirementDescription(int id, String name, String description) {
        this(id, name, description, null);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<UserDescription> getApplicants() {
        return applicants;
    }
}
