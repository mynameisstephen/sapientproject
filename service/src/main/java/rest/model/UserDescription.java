package rest.model;

import model.UserRole;

public class UserDescription {
    private final int id;
    private final String username;
    private final UserRole role;

    public UserDescription(int id, String username, UserRole role) {
        this.id = id;
        this.username = username;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public UserRole getRole() {
        return role;
    }
}
