package rest.model;

public class LoginResponse {
    private final String token;
    private final UserDescription user;

    public LoginResponse(String token, UserDescription user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public UserDescription getUser() {
        return user;
    }
}
