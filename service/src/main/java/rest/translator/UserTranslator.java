package rest.translator;

import model.User;
import rest.model.UserDescription;

public class UserTranslator {

    public static UserDescription translate(User user) {
        return new UserDescription(
                user.getId(),
                user.getUsername(),
                user.getRole()
        );
    }

}
