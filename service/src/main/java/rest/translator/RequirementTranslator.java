package rest.translator;

import model.Requirement;
import rest.model.RequirementDescription;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class RequirementTranslator {
    public static RequirementDescription translate(Requirement requirement, boolean includeApplicants) {
        if (includeApplicants) {
            return new RequirementDescription(
                    requirement.getId(),
                    requirement.getName(),
                    requirement.getDescription(),
                    requirement.getApplicants()
                            .stream()
                            .map(UserTranslator::translate)
                            .collect(Collectors.toCollection(ArrayList::new))
            );
        } else {
            return new RequirementDescription(
                    requirement.getId(),
                    requirement.getName(),
                    requirement.getDescription()
            );
        }
    }

    public static Collection<RequirementDescription> translate(Collection<Requirement> requirements, boolean includeApplicants) {
        return requirements.stream()
                .map((Requirement req) -> RequirementTranslator.translate(req, includeApplicants))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
