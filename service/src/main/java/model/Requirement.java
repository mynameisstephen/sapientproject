package model;

import java.util.ArrayList;

public class Requirement {

    private final int id;
    private final String name;
    private final String description;
    private final ArrayList<User> applicants;

    public Requirement(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.applicants = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<User> getApplicants() {
        return applicants;
    }
}
