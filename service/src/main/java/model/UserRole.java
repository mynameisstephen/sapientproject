package model;

public enum UserRole {
    ADMIN("ADMIN"),
    APPLICANT("APPLICANT");

    private final String role;

    UserRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
