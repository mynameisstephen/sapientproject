## Service

Code is located under `/server`.  It is a standard maven project, use Application as the bootstrap class.

## Client

Code is located under `/client`.  It is a standard npm/yarn project, using typescript, sass and react.  To run, execute `npm run build:dev` to compile and `npm run serve` to launch the dev server.  Application can be found at `http://localhost:8080/deploy/`.