import * as React from 'react';

import Requirement from 'ts/structure/Requirement';

interface RequirementProps { 
    requirement: Requirement;
}

export default (props: RequirementProps) => {
    return (
        <li className="c-requirement-item">
            <h3>{ props.requirement.name }</h3>
            <p>{ props.requirement.description }</p>
            <ul>
                { props.requirement.applicants.map((value) => <li>{ value.username }</li>) }
            </ul>
        </li>
    );
}