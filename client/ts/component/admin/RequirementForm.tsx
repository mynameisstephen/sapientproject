import * as React from 'react';

import APIService from 'ts/service/APIService';
import Requirement from 'ts/structure/Requirement';

interface RequirementFormProps { 
    onRequirementCreated: (requirement: Requirement) => void;
}

interface RequirementFormState { 
    name: string;
    description: string;
    submitting: boolean;
}

export default class RequirementForm extends React.Component<RequirementFormProps, RequirementFormState> {
    constructor(props: RequirementFormProps) {
        super(props);

        this.state = {
            name: '',
            description: '',
            submitting: false
        };
    }

    public render() {
        return (
            <div className="c-requirement-form">
                <h2 className="card-title">Add requirement</h2>
                <form>
                    <label className="c-input-label" htmlFor="name">Requirement name</label>
                    <input className="c-input-field" id="name" type="text" value={ this.state.name } onChange={ this.handlerName.bind(this) } />
                    <label className="c-input-label" htmlFor="description">Requirement description</label>
                    <textarea className="c-textarea-field" id="description" value={ this.state.description } onChange={ this.handlerDescription.bind(this) } />
                    <button
                        className="c-form-button btn btn-lg btn-primary"
                        disabled={ this.state.name == '' || this.state.description == '' || this.state.submitting }
                        onClick={ this.handlerSubmit.bind(this) }                    
                    >Submit</button>
                </form>
            </div>            
        );
    }

    private handlerName(event: React.FormEvent<HTMLInputElement>) {
        this.setState({
            name: event.currentTarget.value
        });
    }

    private handlerDescription(event: React.FormEvent<HTMLTextAreaElement>) {
        this.setState({
            description: event.currentTarget.value
        });
    }

    private async handlerSubmit(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        try {
            this.setState({
                submitting: true
            });

            let result = await APIService.createRequirement({
                name: this.state.name,
                description: this.state.description
            });

            let requirement: Requirement = {
                id: result.id,
                name: this.state.name,
                description: this.state.description,
                applicants: []
            };

            this.setState({
                name: '',
                description: '',
                submitting: false
            });

            this.props.onRequirementCreated(requirement);
        } catch(error) {
            // TODO show error;
            console.log(error);

            this.setState({
                submitting: false
            });            
        }
    }
}