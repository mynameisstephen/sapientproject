import * as React from 'react';

import RequirementForm from 'ts/component/admin/RequirementForm';
import User from 'ts/structure/User';
import Requirement from 'ts/structure/Requirement';
import RequirementItem from 'ts/component/admin/RequirementItem';
import APIService from 'ts/service/APIService';

interface AdminScreenProps {
    user: User;
}

interface AdminScreenState { 
    requirements: Requirement[];
}

export default class AdminScreen extends React.Component<AdminScreenProps, AdminScreenState> {
    constructor(props: AdminScreenProps) {
        super(props);

        this.state = {
            requirements: []
        };
    }

    public async componentDidMount() {
        let requirements = await APIService.getRequirements();

        this.setState({
            requirements: requirements
        });
    }

    public render() {
        return (
            <div className="c-requirement-screen">
                <p className="welcome-message">Hi { this.props.user.username }</p>
                <div className="c-admin-card card">
                    <h2 className="card-title">Active requirements</h2>
                    <ul className="c-requirement-list">
                        { this.state.requirements.map((value) => <RequirementItem requirement={ value } />) }
                        { this.state.requirements.length == 0 && <li className="c-requirement-item">No active requirements</li> }
                    </ul>
                    <hr />
                    <RequirementForm onRequirementCreated={ this.handlerOnRequirementCreated.bind(this) } ></RequirementForm>
                </div>
            </div>
        );
    }

    private handlerOnRequirementCreated(requirement: Requirement) {
        this.setState({
            requirements: this.state.requirements.concat(requirement)
        });
    }
}