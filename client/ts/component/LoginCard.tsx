import * as React from 'react';

import APIService from 'ts/service/APIService';
import User from 'ts/structure/User';

interface LoginCardProps {
    onLogin: (user: User) => void;
}

interface LoginCardState {
    username: string;
    password: string;
    submitting: boolean;
}

export default class LoginCard extends React.Component<LoginCardProps, LoginCardState> {
    constructor(props: LoginCardProps) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitting: false
        }
    }

    public render() {
        return (
            <div className="c-login-card card">
                <h1 className="card-title">Login</h1>
        
                <hr />
        
                <form>
                    <label className="c-input-label" htmlFor="username">Username</label>
                    <input className="c-input-field" type="text" id="username" value={ this.state.username } onChange={ this.handlerUsername.bind(this) } />
                    <label className="c-input-label" htmlFor="password">Password</label>
                    <input className="c-input-field" type="password" id="password" value={ this.state.password } onChange={ this.handlerPassword.bind(this) } />
                    <button 
                        className="c-form-button btn btn-lg btn-primary" 
                        disabled={ this.state.username == '' || this.state.password == '' || this.state.submitting }
                        onClick={ this.handlerSubmit.bind(this) }
                    >Login</button>
                </form>
            </div>
        );
    }

    private handlerUsername(event: React.FormEvent<HTMLInputElement>) {
        this.setState({
            username: event.currentTarget.value 
        });
    }

    private handlerPassword(event: React.FormEvent<HTMLInputElement>) {
        this.setState({
            password: event.currentTarget.value
        });
    }    

    private async handlerSubmit(event: React.MouseEvent<HTMLButtonElement>) { 
        event.preventDefault();

        try {
            this.setState({
                submitting: true
            });

            let result = await APIService.auth({
                username: this.state.username,
                password: this.state.password
            });

            this.setState({
                username: '',
                password: '',
                submitting: false
            });

            this.props.onLogin(result.user);
        } catch (error) {
            // TODO show error;
            console.log(error);

            this.setState({
                submitting: false
            });
        }
    }
}