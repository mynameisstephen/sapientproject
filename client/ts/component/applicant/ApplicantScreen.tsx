import * as React from 'react';

import APIService from 'ts/service/APIService';
import Requirement from 'ts/structure/Requirement';
import RequirementItem from 'ts/component/applicant/RequirementItem';
import User from 'ts/structure/User';

interface ApplicantScreenProps { 
    user: User;    
}

interface ApplicantScreenState { 
    requirements: Requirement[];    
}

export default class ApplicantScreen extends React.Component<ApplicantScreenProps, ApplicantScreenState> {
    constructor(props: ApplicantScreenProps) {
        super(props);

        this.state = { 
            requirements: []
        };
    }

    public async componentDidMount() {
        let requirements = await APIService.getRequirements();

        this.setState({
            requirements: requirements
        });
    }

    public render() {
        return (
            <div className="c-requirement-screen">
                <p className="welcome-message">Hi { this.props.user.username }</p>
                <div className="c-applicant-card card">
                    <h2 className="card-title">Active requirements</h2>
                    <ul className="c-requirement-list">
                        { this.state.requirements.map((value) => <RequirementItem requirement={value} />) }
                        { this.state.requirements.length == 0 && <li className="c-requirement-item">No active requirements</li> }
                    </ul>                
                </div>
            </div>
        );
    }
}