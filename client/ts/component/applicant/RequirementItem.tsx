import * as React from 'react';

import APIService from 'ts/service/APIService';
import Requirement from 'ts/structure/Requirement';

interface RequirementItemProps {
    requirement: Requirement;
}

interface RequirementItemState {
    applied: boolean;
    submitting: boolean;
}

export default class RequirementItem extends React.Component<RequirementItemProps, RequirementItemState> {
    constructor(props: RequirementItemProps) {
        super(props);

        this.state = {
            applied: false,
            submitting: false
        };
    }

    public render() {
        return (
            <li className="c-requirement-item">
                <h3>{ this.props.requirement.name }</h3>
                <p>{ this.props.requirement.description }</p>
                <button
                    className="c-form-button btn btn-lg btn-primary"
                    disabled={ this.state.applied || this.state.submitting }
                    onClick={ this.handlerApply.bind(this) }                    
                >Apply</button>
            </li>
        );
    }

    private async handlerApply(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        try {
            this.setState({
                submitting: true
            });

            let result = await APIService.applyRequirement(this.props.requirement.id);

            this.setState({
                applied: true,
                submitting: false
            });
        } catch (error) {
            // TODO show error;
            console.log(error);

            this.setState({
                submitting: false
            });
        }        
    }
}