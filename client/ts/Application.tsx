import * as React from 'react';

import AdminScreen from 'ts/component/admin/AdminScreen';
import ApplicantScreen from 'ts/component/applicant/ApplicantScreen';
import LoginCard from 'ts/component/LoginCard';
import User from 'ts/structure/User';

interface ApplicationProps { }

interface ApplicationState { 
	user: User;
}

export default class Application extends React.Component<ApplicationProps, ApplicationState> {
	constructor(props: ApplicationProps) {
		super(props);

		this.state = {
			user: null
		};
	}

	public render() {
		let screen: JSX.Element;

		if (this.state.user) {
			if (this.state.user.role == 'ADMIN') {
				screen = <AdminScreen user={ this.state.user } />;
			} else {
				screen = <ApplicantScreen user={ this.state.user } />;
			}
		} else {
			screen = <LoginCard onLogin={this.handlerLogin.bind(this)} />
		}

		return (
			<div className="m-application">
				{ screen }
			</div>
		);
	}

	private handlerLogin(user: User) {
		this.setState({
			user: user
		});
	}
}