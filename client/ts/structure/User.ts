export default interface User {
    id: number;
    username: string;
    role: 'ADMIN' | 'APPLICANT';
}