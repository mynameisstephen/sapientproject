import User from 'ts/structure/User';

export default interface Requirement {
    id: number;
    name: string;
    description: string;
    applicants?: User[];
}