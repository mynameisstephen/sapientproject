import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'sass/main.scss';

import Application from 'ts/Application';
import APIService, { LoginResponse } from 'ts/service/APIService';

export default async function Main() {
	ReactDOM.render(
		<Application />,
		document.getElementById('application')
	);
}

Main();