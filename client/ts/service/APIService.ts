import 'whatwg-fetch';

import User from 'ts/structure/User';
import Requirement from 'ts/structure/Requirement';

const API_BASE_URL = 'http://localhost:9090/'

export interface LoginRequest {
    username: string;
    password: string;
}

export interface LoginResponse {
    token: string;
    user: User
}

export interface CreateRequirementRequest {
    name: string;
    description: string;
}

export interface CreateRequirementResponse {
    id: number;
}

class APIService {

    private token: string;

    public async auth(data: LoginRequest): Promise<LoginResponse> {
        let response = await this.execute(
            'auth',
            'POST',
            data,
            false
        );

        let result: LoginResponse = await response.json();
        this.token = result.token;

        return Promise.resolve(result);
    }

    public async createRequirement(data: CreateRequirementRequest): Promise<CreateRequirementResponse> {
        let response = await this.execute(
            'requirement',
            'POST',
            data
        );

        return Promise.resolve(await response.json());
    }

    public async getRequirements(): Promise<Requirement[]> {
        let response = await this.execute(
            'requirement',
            'GET',
            null
        )

        return Promise.resolve(await response.json());
    }

    public async applyRequirement(requirement: number): Promise<void> {
        let response = await this.execute(
            'requirement/' + requirement + '/apply',
            'POST',
            null
        );

        return Promise.resolve();
    }

    protected async execute(url: string, method: string, data: any, useToken: boolean = true): Promise<Response> {
        let headers = {
            'Content-Type': 'application/json'
        };

        if (useToken) {
            headers['authorization'] = 'Bearer ' + this.token;
        }

        let response: Response = await fetch(
            API_BASE_URL + url,
            {
                method: method,
                headers: headers,
                body: data ? JSON.stringify(data) : null
            }
        );
        
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            throw new Error(response.statusText);
        }
    }
}

export default new APIService();