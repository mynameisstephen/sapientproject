let path = require('path');

let Webpack = require('webpack');
let WebpackMerge = require('webpack-merge');

module.exports = WebpackMerge(require('./webpack.common.js'), {
	'output': {
		'path': path.resolve('./deploy/'),
		'chunkFilename': '[id].chunk.js',
		'filename': '[name].js',
		'pathinfo': true
	},
    'plugins': [
        new Webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new Webpack.optimize.UglifyJsPlugin({
            compress: {
                screw_ie8: true,
                warnings: false
            }
        })
    ]
});