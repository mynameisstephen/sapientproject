let path = require('path');

let Webpack = require('webpack');
let HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	'entry': {
		'app': ['./ts/Main.tsx']
	},
	'resolve': {
		'alias': {
			'sass': path.resolve(__dirname, '../sass'),
			'ts': path.resolve(__dirname, '../ts')
		},
		'extensions': ['.css', '.js', '.scss', '.ts', '.tsx']
	},
	'devServer': {
		contentBase: './deploy'
	},
	'module': {
		'rules': [
			{
				'test': /\.scss$/,
				'loader': 'style-loader!css-loader!sass-loader'
			},
			{
				'test': /\.tsx?$/,
				'loader': 'ts-loader'
			},
			{
				'test': /\.html$/,
				'loader': 'html-loader'
			}
		]
	},
	'plugins': [
		new HTMLWebpackPlugin({
			'template': './html/index.html'
		})
	]
}