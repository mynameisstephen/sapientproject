let path = require('path');

let WebpackMerge = require('webpack-merge');

module.exports = WebpackMerge(require('./webpack.common.js'), {
	'output': {
		'path': path.resolve('./deploy/'),
		'chunkFilename': '[id].chunk.js',
		'filename': '[name].js',
		'pathinfo': true
	}
});